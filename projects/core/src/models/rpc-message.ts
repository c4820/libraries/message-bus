import { v4 as uuid } from 'uuid';

export class RpcMessage<TModel> {
  //#region Properties

  public readonly id: string;

  //#endregion

  //#region Constructor

  public constructor(
    public readonly namespace: string,
    public readonly method: string,
    public data?: TModel,
    id?: string
  ) {
    if (!id || !id.length) {
      this.id = uuid();
      return;
    }

    this.id = id;
  }

  //#endregion
}
