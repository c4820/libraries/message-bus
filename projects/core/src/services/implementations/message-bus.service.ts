import { Injectable, Type } from '@angular/core';
import { Observable, ReplaySubject, Subject, throwError } from 'rxjs';
import { filter, map, switchMap } from 'rxjs/operators';
import { IMessageBusService } from '../interfaces/message-bus-service.interface';
import { MessageContainer } from '../../models/message-container';
import { ChannelInitializationEvent } from '../../models/channel-initialization-event';
import { ExceptionCodes } from '../../constants/exception-codes';
import { IHookChannelOptions } from '../../interfaces/hook-channel-options.interface';
import { CHANNEL_NAME_METADATA, EVENT_NAME_METADATA } from '../../decorators';
import 'reflect-metadata';

@Injectable()
export class MessageBusService implements IMessageBusService {
  //#region Properties

  // Map of channels & event emitter
  private _mChannelEventManager: Map<
    string,
    Map<string, Subject<any>> | undefined
  >;

  // Channel event initialization manager.
  private _mChannelEventInitializationManager: Map<
    string,
    Map<string, Subject<any>>
  >;

  //#endregion

  //#region Constructor

  // Initialize service with injectors.
  public constructor() {
    // Initialize list of channel mappings.
    this._mChannelEventManager = new Map<string, Map<string, Subject<any>>>();
    this._mChannelEventInitializationManager = new Map<
      string,
      Map<string, Subject<any>>
    >();
  }

  //#endregion

  //#region Methods

  // Add message channel event emitter.
  public addMessageChannel<T>(channelName: string, eventName: string): void {
    this.__getMessageChannel<T>(channelName, eventName, true);
  }

  /*
   * Hook message event.
   * Specifying auto create will trigger channel creation if it is not available.
   * Auto create option can cause concurrent issue, such as parent channel can be replaced by child component.
   * Therefore, it should be used wisely.
   * */
  public hookMessageChannel<T>(
    channelName: string,
    eventName: string,
    options?: IHookChannelOptions
  ): Observable<T> {
    const hookedTime = new Date().getTime();

    return this.loadChannelInitializationEventEmitter(
      channelName,
      eventName
    ).pipe(
      // Cancel previous subscription and change to load available message channel.
      map(() => this.__getMessageChannel<T>(channelName, eventName, true)),

      // Jump to event emitter.
      switchMap((messageEmitter: Subject<MessageContainer<T>> | undefined) => {
        // No recipient has been found.
        if (!messageEmitter) {
          return throwError(() => ExceptionCodes.channelNotFound);
        }

        return messageEmitter;
      }),

      // Only pass data back to listener when message is available.
      filter((messageContainer) => {
        return messageContainer && messageContainer.available;
      }),

      // Skip historical messages (if the option is set)
      filter((messageContainer) => {
        // Option is not defined.
        if (!options || !options.skipHistoricalMessages) {
          return true;
        }

        return (
          options.skipHistoricalMessages &&
          messageContainer.createdTime > hookedTime
        );
      }),

      // Pass the data inside the message container to listeners.
      map((messageContainer: MessageContainer<T>) => messageContainer.data)
    );
  }

  // Hook message which is transferred through message bus service.
  public hookMessageChannelByType<T extends object>(
    type: Type<T>,
    options?: IHookChannelOptions
  ): Observable<T> {
    const instance = new type();

    if (
      !Reflect.hasMetadata(CHANNEL_NAME_METADATA, instance) ||
      !Reflect.hasMetadata(EVENT_NAME_METADATA, instance)
    ) {
      throw new Error(
        'Metadata is not found. Did you forget to add MessageEvent decorator on the target class ?'
      );
    }

    // Get the metadata of the class.
    const channelName = Reflect.getMetadata(CHANNEL_NAME_METADATA, instance);
    const eventName = Reflect.getMetadata(EVENT_NAME_METADATA, instance);

    return this.hookMessageChannel<T>(channelName, eventName, options);
  }

  /*
   * Publish message to event stream.
   * Channel will be created automatically if it isn't available.
   * */
  public addMessage<T>(
    channelName: string,
    eventName: string,
    data: T,
    lifetimeInSeconds?: number
  ): void {
    const emitter = this.__getMessageChannel(channelName, eventName, true);

    // Build message container.
    const messageContainer = new MessageContainer<T>(
      data,
      true,
      lifetimeInSeconds
    );

    if (!emitter) {
      return;
    }
    emitter.next(messageContainer);
  }

  /*
   * Publish message to event stream.
   * Channel will be created automatically if it isn't available.
   * */
  public addMessageInstance<T extends object>(message: T): void {
    // Get the metadata of the class.
    const channelName = Reflect.getMetadata(CHANNEL_NAME_METADATA, message);
    const eventName = Reflect.getMetadata(EVENT_NAME_METADATA, message);

    if (
      !Reflect.hasMetadata(CHANNEL_NAME_METADATA, message) ||
      !Reflect.hasMetadata(EVENT_NAME_METADATA, message)
    ) {
      throw new Error(
        'Metadata is not found. Did you forget to add MessageEvent decorator on the target class ?'
      );
    }

    this.addMessage(channelName, eventName, message);
  }

  // Delete messages that have been sent.
  public deleteChannelMessage(channelName: string, eventName: string): void {
    const channelMessageEmitter = this.__getMessageChannel(
      channelName,
      eventName,
      false
    );
    if (channelMessageEmitter == null) {
      return;
    }

    // Initialize unavailable message container.
    const messageContainer = new MessageContainer<any>(null, false, undefined);

    // Emit the blank message to channel.
    channelMessageEmitter.next(messageContainer);
  }

  // Delete messages that have been sent through a specific channel & event.
  public deleteMessageByType<T>(type: Type<T>): void {
    const instance = new type();
    const metadata = this._loadMetadata(instance);

    this.deleteChannelMessage(metadata.channelName, metadata.eventName);
  }

  // Delete message from every channel.
  public deleteMessages(): void {
    if (this._mChannelEventManager == null) {
      return;
    }

    // Go through every channel.
    const channelNames = this._mChannelEventManager.keys();
    for (const channelName of channelNames) {
      const channelNameEventNameMap =
        this._mChannelEventManager.get(channelName);
      if (!channelNameEventNameMap) {
        continue;
      }

      // Get event names.
      const eventNames = channelNameEventNameMap.keys();
      for (const eventName of eventNames) {
        // Get message emitter.
        const emitter = channelNameEventNameMap.get(eventName);

        // Initialize blank message.
        const messageContainer = new MessageContainer<any>(
          null,
          false,
          undefined
        );
        if (!emitter) {
          continue;
        }
        emitter.next(messageContainer);
      }
    }
  }

  // Hook to channel channel - event initialization.
  public hookChannelInitialization(
    channelName: string,
    eventName: string
  ): Observable<ChannelInitializationEvent> {
    return this.loadChannelInitializationEventEmitter(channelName, eventName);
  }

  // Hook channel initialization by using typed declaration.
  public hookChannelInitializationByType<T>(
    type: Type<T>
  ): Observable<ChannelInitializationEvent> {
    const instance = new type();
    const metadata = this._loadMetadata(instance);
    return this.hookChannelInitialization(
      metadata.channelName,
      metadata.eventName
    );
  }

  /*
   * Load message channel using channel name and event name.
   * Specifying auto create will trigger channel creation if it is not available.
   * Auto create option can cause concurrent issue, such as parent channel can be replaced by child component.
   * Therefore, it should be used wisely.
   * */
  protected __getMessageChannel<T>(
    channelName: string,
    eventName: string,
    autoCreate?: boolean
  ): Subject<MessageContainer<T>> | undefined {
    let mChannelEventManager = this._mChannelEventManager;

    if (mChannelEventManager == null) {
      // Cannot create channel automatically.
      if (!autoCreate) {
        return undefined;
      }

      mChannelEventManager = new Map<string, Map<string, Subject<any>>>();
      this._mChannelEventManager = mChannelEventManager;
    }

    if (!mChannelEventManager.has(channelName)) {
      // Cannot create channel automatically.
      if (!autoCreate) {
        return undefined;
      }
      mChannelEventManager.set(channelName, undefined);
    }

    // Channel is not found.
    let channelNameMessageEmitterMap = mChannelEventManager.get(channelName);
    if (channelNameMessageEmitterMap == null) {
      if (!autoCreate) {
        return undefined;
      }

      channelNameMessageEmitterMap = new Map<string, Subject<any>>();
      mChannelEventManager.set(channelName, channelNameMessageEmitterMap);
    }

    let messageEmitter = channelNameMessageEmitterMap.get(eventName);
    let hasMessageEmitterAdded = false;

    if (messageEmitter == null) {
      if (!autoCreate) {
        return undefined;
      }

      // Mark the channel to be added.
      hasMessageEmitterAdded = true;

      messageEmitter = new ReplaySubject<MessageContainer<T>>(1);
      channelNameMessageEmitterMap.set(eventName, messageEmitter);
    }

    // Raise an event about message channel creation if it has been newly added.
    if (hasMessageEmitterAdded) {
      // Raise an event about newly created channel.
      const channelInitializationEmitter =
        this.loadChannelInitializationEventEmitter(channelName, eventName);
      channelInitializationEmitter.next(
        new ChannelInitializationEvent(channelName, eventName)
      );
    }

    return messageEmitter;
  }

  // Load channel initialization event emitter.
  protected loadChannelInitializationEventEmitter(
    channelName: string,
    eventName: string
  ): Subject<ChannelInitializationEvent> {
    if (!this._mChannelEventInitializationManager.has(channelName)) {
      this._mChannelEventInitializationManager.set(
        channelName,
        new Map<string, Subject<any>>()
      );
    }

    let eventInitializationManager =
      this._mChannelEventInitializationManager.get(channelName);
    if (eventInitializationManager == null) {
      eventInitializationManager = new Map<string, Subject<any>>();
      this._mChannelEventInitializationManager.set(
        channelName,
        eventInitializationManager
      );
    }

    // Get the event emitter.
    let eventEmitter = eventInitializationManager.get(eventName);
    if (!eventEmitter) {
      eventEmitter = new ReplaySubject<ChannelInitializationEvent>(1);
      eventInitializationManager.set(eventName, eventEmitter);
      this._mChannelEventInitializationManager.set(
        channelName,
        eventInitializationManager
      );
    }

    return eventEmitter;
  }

  protected _loadMetadata(instance: any): {
    channelName: string;
    eventName: string;
  } {
    if (
      !Reflect.hasMetadata(CHANNEL_NAME_METADATA, instance) ||
      !Reflect.hasMetadata(EVENT_NAME_METADATA, instance)
    ) {
      throw new Error(
        'Metadata is not found. Did you forget to add MessageEvent decorator on the target class ?'
      );
    }

    // Get the metadata of the class.
    const channelName = Reflect.getMetadata(CHANNEL_NAME_METADATA, instance);
    const eventName = Reflect.getMetadata(EVENT_NAME_METADATA, instance);

    return { channelName, eventName };
  }

  //#endregion
}
