import { Observable } from 'rxjs';
import { IHookMethodRequestOptions } from '../../interfaces/hook-method-request-options';
import { RpcMessage } from '../../models';
import { Type } from '@angular/core';

export interface IRpcService {
  //#region Methods

  // Call a remote function asynchronously.
  sendRequestAsync<TRequest extends object, TResponse>(
    data: TRequest,
    timeoutInMilliseconds?: number
  ): Observable<TResponse>;

  // Send a response from requested function
  sendResponseByType<TRequest extends object, TResponse>(
    type: Type<TRequest>,
    messageId: string,
    data: TResponse
  ): void;

  sendResponse<TResponse>(
    namespace: string,
    method: string,
    messageId: string,
    data: TResponse
  ): void;

  sendExceptionByType<TRequest extends object, TException>(
    type: Type<TRequest>,
    messageId: string,
    exception: TException
  ): void;

  sendException<TException>(
    namespace: string,
    method: string,
    messageId: string,
    exception: TException
  ): void;

  // Hook method request asynchronously
  hookMethodRequestAsync<TRequest extends object, TResponse>(
    type: Type<TRequest>,
    options?: IHookMethodRequestOptions
  ): Observable<RpcMessage<TResponse>>;

  hookMethodsRequestsAsync<TResponse>(): Observable<RpcMessage<TResponse>>;

  //#endregion
}
