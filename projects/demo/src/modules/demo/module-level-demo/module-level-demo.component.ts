import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
  selector: 'singleton-service-demo',
  templateUrl: './module-level-demo.component.html',
  styleUrls: ['./module-level-demo.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModuleLevelDemoComponent {

  //#region Constructor

  //#endregion

  //#region Methods


  //#endregion
}
