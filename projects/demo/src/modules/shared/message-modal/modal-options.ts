export class ModalOptions {

  //#region Properties

  public title: string;

  public content: string;

  //#endregion

}
